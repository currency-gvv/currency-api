INSERT INTO exchange_value (id, currency_from, currency_to, conversion_multiple, port)
VALUES (10001, 'USD', 'COP', 65, 0);
INSERT INTO exchange_value (id, currency_from, currency_to, conversion_multiple, port)
VALUES (10002, 'EUR', 'COP', 75, 0);
INSERT INTO exchange_value (id, currency_from, currency_to, conversion_multiple, port)
VALUES (10003, 'AUD', 'COP', 25, 0);